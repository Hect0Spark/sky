<?php

/*
Upload file script
This php script uploads xml files to the data directory and renames the file to data.xml
*/
  if(isset($_POST['submit'])){
    //Get file properites
    $file = $_FILES['file']; //Get file array
    $filename = $_FILES['file']['name']; //get filename
    $fileTmpName = $_FILES['file']['tmp_name']; //get temp file location
    $fileType = $_FILES['file']['type']; //get file type
    $fileError = $_FILES['file']['error'];

    $fileExt = explode('.', $filename); //split filename into array
    $fileActualExt = strtolower(end($fileExt)); //get extension at the end of the array and put to lowercase.

    $allowed = array('xml'); //allowed files


    if (in_array($fileActualExt, $allowed)){
      if ($fileError === 0){
        if($fileSize < 5000){ //Max 5MB
          $fileDestination = 'data/data.xml';//set filename and destination, we want the filename to always be the same
          move_uploaded_file($fileTmpName, $fileDestination); //Move file from temporary storage
          header("Location: index.php?uploadsuccess"); //Go to moodslider with a URL buzzword to let the user know it has been a success.
        } else{ echo "File too large. Please upload smaller than 5MB";} //If bigger than 5MB

      }else{ echo "File Upload Error";} //if error then echo
    } else{
      echo "Please upload an .xml file only.";
    }
  }
   ?>
