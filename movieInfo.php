<?php
/*
Movie Information page
This page displays the description, age rating, and movie cover of the movie.
This allows the user to decided if this is the movie they would like to watch.
*/
if(isset($_GET['index'])){
$index = $_GET['index']; //movie xml index
settype($index, "integer"); //set index as int so it can be used with arrays.
//load xml
$xml = simplexml_load_file('./data/data.xml');
//get variables
$movieName = $xml->programme[$index]->name;
$movieRating = $xml->programme[$index]->agerating;
$movieImage = $xml->programme[$index]->imagepath;
$movieDescription = $xml->programme[$index]->description;

//set rating image
switch($movieRating){
  case "U";
  $ratingImage = "images/BBFC_U.jpg";
  break;
  case "PG";
  $ratingImage = "images/BBFC_PG.jpg";
  break;
  case "12A";
  $ratingImage = "images/BBFC_12A.jpg";
  break;
  case "12";
  $ratingImage = "images/BBFC_12.jpg";
  break;
  case "15";
  $ratingImage = "images/BBFC_15.jpg";
  break;
  case "18";
  $ratingImage = "images/BBFC_18.jpg";
  break;
}
}
else{
  echo "GET error";
//set defaults
$movieName = "no content";
$movieRating = "";
$movieImage = "no content";
$movieDescription = "images/noimage.jpg";
}




 ?>
<head>
<meta charset="UTF-8">
<title> Movie Information - Moodslider </title>
<link rel="stylesheet" href="styles.css">
</head>

<table style="height: 228px;" border="1" width="536">
<tbody>
<tr style="height: 22px;">
<td style="width: 100px; text-align: center; height: 44px;" rowspan="2"><img src="images/sky-logo.jpg" alt="sky logo" width="100" /></td>
<td style="text-align: center; height: 22px;" colspan="4style=&quot;width:">Movie Information - <?php echo htmlentities($movieName); ?></td>
</tr>
<tr style="height: 22px;">
<td style="height: 22px;" colspan="4style=&quot;width:"><a href="index.php">Moodslider</a> | <a href="uploadData.php">Upload content</a></td>
</tr>
<tr style="height: 22px;">
<td style="width: 100px; text-align: center; height: 22px;"><img src="<?php echo htmlentities($movieImage); ?>" alt="MovieBox" width="100" /></td>
<td style="width: 100px; height: 160px;" colspan="3"><?php echo htmlentities($movieDescription); ?></td>
<td style="width: 100px; text-align: center; height: 22px;"><img src="<?php echo htmlentities($ratingImage); ?>" alt="Rating" width="100" /></td>
</tr>
</tbody>
</table>
