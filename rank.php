<?php
/*
Explanation behind movie ranking.
I decided to solve the issue of ranking movies depending on multiple slider scores.
Each movie is has a score out of a 100 representing each slider.
The difference then between each slider score and the user slider is found.
All the differences are added up and the movie with the lowest amount of difference total becomes the users top recommendation.
*/
//CHECK IF POST
if(isset($_POST['submit'])){
//Grab Variables
$agitatedCalmValue = $_POST['AgitatedCalm'];
$happySadValue = $_POST['HappySad'];
$tiredAwakeValue = $_POST['TiredAwake'];
$scaredFearlessValue = $_POST['ScaredFearless'];
//Set variables as integers
settype($agitatedCalmValue, "integer");
settype($happySadValue, "integer");
settype($tiredAwakeValue, "integer");
settype($scaredFearlessValue, "integer");

//Load XML
//example of getting xml => $xml->programme[1]->name;
$xml = simplexml_load_file('./data/data.xml');
//Count how many programmes are in xml.
$programmeCount = $xml->count();
//Create Arrays
$scoreArray = array();
$scoreIndexArray = array();
//Calculate scores
for($x = 0; $x < $programmeCount; $x = $x+1){
  //get scores from xml
  $acScore = $xml->programme[$x]->AgitatedCalmScore;
  $hsScore = $xml->programme[$x]->HappySadScore;
  $taScore = $xml->programme[$x]->TiredAwakeScore;
  $scScore = $xml->programme[$x]->ScaredFearlessScore;
  //calculate difference
  $acDiff = abs($acScore - $agitatedCalmValue);
  $hsDiff = abs($hsScore - $happySadValue);
  $taDiff = abs($taScore - $tiredAwakeValue);
  $scDiff = abs($scScore - $scaredFearlessValue);

  //calculate final score of movie
  $finalScore = $acDiff + $hsDiff + $taDiff + $scDiff;
  //store in array
  $scoreArray += [$x => $finalScore];
}
asort($scoreArray); //Sort array
// /print_r($scoreArray);

$scoreArrayKeys = array_keys($scoreArray); //Gather keys
//send as GET to the homepage with rankings and slider positions.
header("Location: index.php?index0=".$scoreArrayKeys[0]."&index1=".$scoreArrayKeys[1]."&index2=".$scoreArrayKeys[2]."&index3=".$scoreArrayKeys[3]."&index4=".$scoreArrayKeys[4]."&AgitatedCalm=".$agitatedCalmValue."&HappySad=".$happySadValue."&TiredAwake=".$tiredAwakeValue."&ScaredFearless=".$scaredFearlessValue);


}else{echo "POST ERROR";}
 ?>
