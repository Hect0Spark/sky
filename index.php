<?php
/*
Home Page Movie Sliders
This is the main user interface. It is comprised of 4 sliders for the user to select their mood.
This then gets compiled into options of movies for the user to choose from. When the user clicks the box it will take them to an information page.

How to use:
1. Move sliders on your mood is
2. Click Submit at the bottom
3. Select a movie and read the description

This is a working model. If updated it would be possible to update the UI to be more modern and remove the submit button to allow the sliders to be self updating.
*/
if(isset($_GET['index0'])){
//Grab Variables
$agitatedCalmValue = $_GET['AgitatedCalm'];
$happySadValue = $_GET['HappySad'];
$tiredAwakeValue = $_GET['TiredAwake'];
$scaredFearlessValue = $_GET['ScaredFearless'];
//Grab Xml Index Values
$index0 = $_GET['index0'];
$index1 = $_GET['index1'];
$index2 = $_GET['index2'];
$index3 = $_GET['index3'];
$index4 = $_GET['index4'];
//Set variables as integers
settype($agitatedCalmValue, "integer");
settype($happySadValue, "integer");
settype($tiredAwakeValue, "integer");
settype($scaredFearlessValue, "integer");
settype($index0, "integer");
settype($index1, "integer");
settype($index2, "integer");
settype($index3, "integer");
settype($index4, "integer");
//load xml
$xml = simplexml_load_file('./data/data.xml');
//set movie variables
//Movie Names
$movieName0 = $xml->programme[$index0]->name;
$movieName1 = $xml->programme[$index1]->name;
$movieName2 = $xml->programme[$index2]->name;
$movieName3 = $xml->programme[$index3]->name;
$movieName4 = $xml->programme[$index4]->name;
//Movie Images
$movieImage0 = $xml->programme[$index0]->imagepath;
$movieImage1 = $xml->programme[$index1]->imagepath;
$movieImage2 = $xml->programme[$index2]->imagepath;
$movieImage3 = $xml->programme[$index3]->imagepath;
$movieImage4 = $xml->programme[$index4]->imagepath;

}else{
  //Set default values
  $agitatedCalmValue = 50;
  $happySadValue = 50;
  $tiredAwakeValue = 50;
  $scaredFearlessValue = 50;
  $movieName0 = "No content";
  $movieName1 = "No content";
  $movieName2 = "No content";
  $movieName3 = "No content";
  $movieName4 = "No content";
  $movieImage0 = "images/noimage.jpg";
  $movieImage1 = "images/noimage.jpg";
  $movieImage2 = "images/noimage.jpg";
  $movieImage3 = "images/noimage.jpg";
  $movieImage4 = "images/noimage.jpg";
}


 ?>
<head>
<meta charset="UTF-8">
<title> Home - Moodslider </title>
<link rel="stylesheet" href="styles.css">
</head>

<html>
<body>

<table style="height: 228px;" border="1" width="536">
<tbody>
<tr style="height: 22px;">
<td style="width: 100px; text-align: center; height: 44px;" rowspan="2"><img src = "images/sky-logo.jpg" alt="sky logo" width = "100"></td>
<td style="text-align: center; height: 22px;" colspan="4style=&quot;width:">Moodslider</td>
</tr>
<tr style="height: 22px;">
<td style="height: 22px;" colspan="4 style=&quot;width:">Moodslider | <a href="uploadData.php">Upload content</a></td></tr>
<form action="rank.php" method="POST">
<tr style="height: 22px;">
<td style="width: 100px; text-align: center; height: 22px;">Agitated</td>
<td style="width: 100px; text-aligh: center; height: 22px;" colspan="3">
  <div class="slidecontainer">
  <input type="range" name="AgitatedCalm" value="<?php echo htmlentities($agitatedCalmValue); ?>" class="slider"/></div></td>
<td style="width: 100px; text-align: center; height: 22px;">Calm</td>
</tr>
<tr style="height: 22px;">
<td style="width: 100px; text-align: center; height: 22px;">Happy&nbsp;</td>
<td style="width: 100px; height: 22px;" colspan="3">
<div class="slidecontainer">
<input type="range" name="HappySad" value="<?php echo htmlentities($happySadValue); ?>" class="slider"/></div>
</td>
<td style="width: 100px; text-align: center; height: 22px;">Sad</td>
</tr>
<tr style="height: 22px;">
<td style="width: 100px; text-align: center; height: 22px;">Tired&nbsp;</td>
<td style="width: 100px; height: 22px;" colspan="3"><div class="slidecontainer">
<input type="range" name="TiredAwake" value="<?php echo htmlentities($tiredAwakeValue); ?>" class="slider"/></div></td>
<td style="width: 100px; text-align: center; height: 22px;">Wide Awake&nbsp;</td>
</tr>
<tr style="height: 22px;">
<td style="width: 100px; text-align: center; height: 22px;">Scared&nbsp;</td>
<td style="width: 100px; height: 22px;" colspan="3"><div class="slidecontainer">
<input type="range" name="ScaredFearless" value="<?php echo htmlentities($scaredFearlessValue); ?>" class="slider"/></div></td>
<td style="width: 100px; text-align: center; height: 22px;">Fearless&nbsp;</td>
</tr>
<tr style="height: 91px;">
<td style="width: 100px; height: 147px;"><a href="movieInfo.php?index=<?php echo htmlentities($index0); ?>"><img src = "<?php echo htmlentities($movieImage0); ?>" alt="No Content" width = "100"></a></td>
<td style="width: 100px; height: 147px;"><a href="movieInfo.php?index=<?php echo htmlentities($index1); ?>"><img src = "<?php echo htmlentities($movieImage1); ?>" alt="No Content" width = "100"></a></td>
<td style="width: 100px; height: 147px;"><a href="movieInfo.php?index=<?php echo htmlentities($index2); ?>"><img src = "<?php echo htmlentities($movieImage2); ?>" alt="No Content" width = "100"></a></td>
<td style="width: 101px; height: 147px;"><a href="movieInfo.php?index=<?php echo htmlentities($index3); ?>"><img src = "<?php echo htmlentities($movieImage3); ?>" alt="No Content" width = "100"></a></td>
<td style="width: 101px; height: 147px;"><a href="movieInfo.php?index=<?php echo htmlentities($index4); ?>"><img src = "<?php echo htmlentities($movieImage4); ?>" alt="No Content" width = "100"></a></td>
</tr>
<tr style="height: 22px;">
<td style="width: 100px; height: 22px;"><a href="movieInfo.php?index=<?php echo htmlentities($index0); ?>"><?php echo htmlentities($movieName0); ?></a></td>
<td style="width: 100px; height: 22px;"><a href="movieInfo.php?index=<?php echo htmlentities($index1); ?>"><?php echo htmlentities($movieName1); ?></a></td>
<td style="width: 100px; height: 22px;"><a href="movieInfo.php?index=<?php echo htmlentities($index2); ?>"><?php echo htmlentities($movieName2); ?></a></td>
<td style="width: 101px; height: 22px;"><a href="movieInfo.php?index=<?php echo htmlentities($index3); ?>"><?php echo htmlentities($movieName3); ?></a></td>
<td style="width: 101px; height: 22px;"><a href="movieInfo.php?index=<?php echo htmlentities($index4); ?>"><?php echo htmlentities($movieName4); ?></a></td>
</tr>
</tbody>
</table>

<input type = "submit" name="submit" value="SUBMIT" />
</form>

</body>

</html>
